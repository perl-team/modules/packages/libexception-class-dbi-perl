libexception-class-dbi-perl (1.04-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 30 Jun 2022 20:37:42 +0100

libexception-class-dbi-perl (1.04-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 1.04.
  * Update years of upstream copyright.
  * Update build dependencies.
  * Update debian/upstream/metadata.
  * Annotate test-only build dependencies with <!nocheck>.
  * Declare compliance with Debian Policy 4.4.1.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.

 -- gregor herrmann <gregoa@debian.org>  Tue, 05 Nov 2019 21:13:17 +0100

libexception-class-dbi-perl (1.01-1) unstable; urgency=low

  [ Nicholas Bamber ]
  * New upstream release

  [ gregor herrmann ]
  * Remove alternative (build) dependencies that are already satisfied
    in oldstable.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * debian/control: remove Nicholas Bamber from Uploaders on request of
    the MIA team.
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Add explicit build dependency on libmodule-build-perl.
  * Update years of upstream copyright.
  * Bump debhelper compatibility level to 9.
  * Add /me to Uploaders.
  * Mention module name in long description.

 -- gregor herrmann <gregoa@debian.org>  Tue, 09 Jun 2015 21:46:25 +0200

libexception-class-dbi-perl (1.00-1) unstable; urgency=low

  [ Nicholas Bamber ]
  * Initial Release. (Closes: #510074)
  * B-D-I on libtest-pod-perl to enable pod tests.

  [ Tim Retout ]
  * debian/source/format: Set to '3.0 (quilt)'.
  * debian/copyright: Add missing upstream copyright years.
  * Avoid shipping uninteresting README in docs.

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Tue, 10 Aug 2010 14:59:29 +0100
